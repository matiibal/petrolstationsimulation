package org.example.carWashQueue;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.example.dao.Customer;
import org.example.dao.ExternalEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.LinkedList;

import static org.example.util.PorticoUtil.convertInterval;
import static org.example.util.PorticoUtil.convertTime;

public class CarWashQueueFederate {
    public static final String READY_TO_RUN = "ReadyToRun";
    private final double timeStep = 1.0;
    protected LinkedList<Customer> customerList = new LinkedList<>();
    protected int timeHlaHandle = 0;
    protected int queueHlaHandle = 0;
    private RTIambassador rtiAmbassador;
    private CarWashQueueAmbassador carWashQueueAmbassador;

    @SneakyThrows
    public static void main(String[] args) {
        new CarWashQueueFederate().runFederate();
    }

    @SneakyThrows
    public void runFederate() {
        rtiAmbassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
        try {
            val fom = new File("station.fed");
            rtiAmbassador.createFederationExecution("Federation - Station", fom.toURI().toURL());
            log("Created Federation");
        } catch (FederationExecutionAlreadyExists exists) {
            log("Didn't create federation, it already existed");
        } catch (MalformedURLException urle) {
            log("Exception processing fom: " + urle.getMessage());
            urle.printStackTrace();
            return;
        }
        carWashQueueAmbassador = new CarWashQueueAmbassador(this);
        rtiAmbassador.joinFederationExecution("CarWashQueueFederate", "Federation - Station", carWashQueueAmbassador);
        log("Joined Federation as org.example.customerQueue.CustomerQueueFederate");
        rtiAmbassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);
        while (!carWashQueueAmbassador.isAnnounced) rtiAmbassador.tick();

        waitForUser();

        rtiAmbassador.synchronizationPointAchieved(READY_TO_RUN);
        log("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");
        while (!carWashQueueAmbassador.isReadyToRun) rtiAmbassador.tick();

        enableTimePolicy();

        publishAndSubscribe();

        registerObject();

        while (carWashQueueAmbassador.running) {
            var timeToAdvance = carWashQueueAmbassador.federateTime + timeStep;
            advanceTime(timeToAdvance);
            if (carWashQueueAmbassador.externalEvents.size() > 0) {
                carWashQueueAmbassador.externalEvents.sort(new ExternalEvent.ExternalEventComparator());
                for (ExternalEvent externalEvent : carWashQueueAmbassador.externalEvents) {
                    carWashQueueAmbassador.federateTime = externalEvent.getTime();
                    switch (externalEvent.getEventType()) {
                        case ADD_CUSTOMER_TO_CAR_WASH_QUEUE:
                            addCustomerToCarWashQueue(externalEvent.getCustomerId());
                            break;
                        case DELETE_CUSTOMER_FROM_CAR_WASH_QUEUE:
                            if (!customerList.isEmpty()) {
                                addCustomerToCashDeskQueue(timeToAdvance + timeStep, "CashDesk");
                                getCustomerFromQueue(timeToAdvance + timeStep);
                                break;
                            }
                    }
                }
                carWashQueueAmbassador.externalEvents.clear();
            }
            if (carWashQueueAmbassador.grantedTime == timeToAdvance) {
                timeToAdvance += carWashQueueAmbassador.federateLookahead;
                updateQueueSize(timeToAdvance);
                carWashQueueAmbassador.federateTime = timeToAdvance;
            }
            rtiAmbassador.tick();
        }
    }

    @SneakyThrows
    private void getCustomerFromQueue(double time){
        if (customerList.size() > 0) {
            val customer = customerList.getFirst();
            customer.setRemoveTime(carWashQueueAmbassador.federateTime);
            customer.calculateWaitingTime();
            val waitingTime = customer.getWaitingTime();
            this.updateWaitingTime(time, waitingTime);
            customerList.removeFirst();
            log("Usunięcie klienta z myjni, aktualny stan kolejki: " + customerList.size());
        }
    }

    private void addCustomerToCarWashQueue(int id) {
        val customer = new Customer(id, carWashQueueAmbassador.federateTime);
        customerList.add(customer);
        log("Dodano klienta do myjni");
    }

    @SneakyThrows
    private void addCustomerToCashDeskQueue(double timeToAdvance, String nameQueue) {
        val customer = customerList.getFirst();
        val parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
        byte[] idCustomer = EncodingHelpers.encodeInt(customer.getId());
        int interactionHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.AddCustomerTo" + nameQueue + "Queue");
        int customerHandle = rtiAmbassador.getParameterHandle("idCustomer", interactionHandle);

        parameters.add(customerHandle, idCustomer);
        LogicalTime time = convertTime(timeToAdvance);
        rtiAmbassador.sendInteraction(interactionHandle, parameters, "tag".getBytes(), time);

        log("Dodanie klienta do " + nameQueue + " kolejki, akturalny stan klientów myjni: " + customerList.size());
    }

    private void updateQueueSize(double time) throws RTIexception {
        log("Aktualny stan kolejki wynosi: " + customerList.size());
        val attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();

        val classHandle = rtiAmbassador.getObjectClass(queueHlaHandle);
        val queueHandle = rtiAmbassador.getAttributeHandle("size", classHandle);
        val queueSize = EncodingHelpers.encodeInt(customerList.size());

        attributes.add(queueHandle, queueSize);
        val logicalTime = convertTime(time);
        rtiAmbassador.updateAttributeValues(queueHlaHandle, attributes, "actualize queue".getBytes(), logicalTime);
    }

    private void updateWaitingTime(double time, double waitingTime) throws RTIexception {
        val attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();
        val classHandle = rtiAmbassador.getObjectClass(timeHlaHandle);
        val timeHandle = rtiAmbassador.getAttributeHandle("time", classHandle);
        val waitingTimeByte = EncodingHelpers.encodeDouble(waitingTime);
        attributes.add(timeHandle, waitingTimeByte);
        val logicalTime = convertTime(time);
        rtiAmbassador.updateAttributeValues(timeHlaHandle, attributes, "actualize queue".getBytes(), logicalTime);
    }


    private void registerObject() throws RTIexception {
        var classHandle = rtiAmbassador.getObjectClassHandle("ObjectRoot.QueueCarWash");
        this.queueHlaHandle = rtiAmbassador.registerObjectInstance(classHandle);

        classHandle = rtiAmbassador.getObjectClassHandle("ObjectRoot.WaitingTimeToCarWash");
        this.timeHlaHandle = rtiAmbassador.registerObjectInstance(classHandle);
    }

    private void publishAndSubscribe() throws RTIexception {
        var classHandle = rtiAmbassador.getObjectClassHandle("ObjectRoot.QueueCarWash");
        var sizeHandle = rtiAmbassador.getAttributeHandle("size", classHandle);

        var attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(sizeHandle);

        rtiAmbassador.publishObjectClass(classHandle, attributes);

        classHandle = rtiAmbassador.getObjectClassHandle("ObjectRoot.WaitingTimeToCarWash");
        sizeHandle = rtiAmbassador.getAttributeHandle("time", classHandle);

        attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(sizeHandle);

        rtiAmbassador.publishObjectClass(classHandle, attributes);

        val addCustomerHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.AddCustomerToCarWashQueue");
        carWashQueueAmbassador.addCustomerHandle = addCustomerHandle;
        rtiAmbassador.subscribeInteractionClass(addCustomerHandle);

        val getCustomerHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.GetCustomerFromCarWashQueue");
        carWashQueueAmbassador.getCustomerHandle = getCustomerHandle;
        rtiAmbassador.subscribeInteractionClass(getCustomerHandle);

        val addCustomerToCashDeskHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.AddCustomerToCashDeskQueue");
        carWashQueueAmbassador.addCustomerToCashDeskHandle = addCustomerToCashDeskHandle;
        rtiAmbassador.publishInteractionClass(addCustomerToCashDeskHandle);

    }

    @SneakyThrows
    private void advanceTime(double timeToAdvance)  {
        carWashQueueAmbassador.isAdvancing = true;
        val newTime = convertTime(timeToAdvance);
        rtiAmbassador.timeAdvanceRequest(newTime);
        while (carWashQueueAmbassador.isAdvancing) rtiAmbassador.tick();
    }

    private void enableTimePolicy() throws RTIexception {
        val currentTime = convertTime(carWashQueueAmbassador.federateTime);
        val lookahead = convertInterval(carWashQueueAmbassador.federateLookahead);
        this.rtiAmbassador.enableTimeRegulation(currentTime, lookahead);
        while (!carWashQueueAmbassador.isRegulating) rtiAmbassador.tick();

        this.rtiAmbassador.enableTimeConstrained();
        while (!carWashQueueAmbassador.isConstrained) rtiAmbassador.tick();
    }

    private void waitForUser() {
        log(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        val reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            log("Error while waiting for user input: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void log(String message) {
        System.out.println("CarWashQueueFederate: " + message);
    }

}
