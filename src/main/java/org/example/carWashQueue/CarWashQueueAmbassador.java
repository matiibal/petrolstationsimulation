package org.example.carWashQueue;

import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;
import hla.rti.ReceivedInteraction;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.NullFederateAmbassador;
import lombok.SneakyThrows;
import lombok.val;
import org.example.dao.ExternalEvent;
import org.portico.impl.hla13.types.DoubleTime;

import java.util.ArrayList;

public class CarWashQueueAmbassador extends NullFederateAmbassador {

    //----------------------------------------------------------
    //                    STATIC VARIABLES
    //----------------------------------------------------------

    //----------------------------------------------------------
    //                   INSTANCE VARIABLES
    //----------------------------------------------------------
    // these variables are accessible in the package
    protected double federateTime = 0.0;
    protected double grantedTime = 0.0;
    protected double federateLookahead = 1.0;

    protected boolean isRegulating = false;
    protected boolean isConstrained = false;
    protected boolean isAdvancing = false;

    protected boolean isAnnounced = false;
    protected boolean isReadyToRun = false;

    protected boolean running = true;
    protected int addCustomerHandle = 0;
    protected int getCustomerHandle = 1;
    protected int addCustomerToCashDeskHandle;
    protected ArrayList<ExternalEvent> externalEvents = new ArrayList<>();

    protected CarWashQueueFederate fed;

    public CarWashQueueAmbassador(CarWashQueueFederate fed) {
        this.fed = fed;
    }

    private double convertTime(LogicalTime logicalTime) {
        return ((DoubleTime) logicalTime).getTime();
    }

    private void log(String message) {
        System.out.println("CarWashQueueAmbassador: " + message);
    }

    public void synchronizationPointRegistrationFailed(String label) {
        log("Failed to register sync point: " + label);
    }

    public void synchronizationPointRegistrationSucceeded(String label) {
        log("Successfully registered sync point: " + label);
    }

    public void announceSynchronizationPoint(String label, byte[] tag) {
        log("Synchronization point announced: " + label);
        if (label.equals(CarWashQueueFederate.READY_TO_RUN)) this.isAnnounced = true;
    }

    public void federationSynchronized(String label) {
        log("Federation Synchronized: " + label);
        if (label.equals(CarWashQueueFederate.READY_TO_RUN)) this.isReadyToRun = true;
    }

    public void timeRegulationEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTime(theFederateTime);
        this.isRegulating = true;
    }

    public void timeConstrainedEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTime(theFederateTime);
        this.isConstrained = true;
    }

    public void timeAdvanceGrant(LogicalTime theTime) {
        this.grantedTime = convertTime(theTime);
        this.isAdvancing = false;
    }

    public void receiveInteraction(int interactionClass, ReceivedInteraction theInteraction,
                                   byte[] tag) {
        receiveInteraction(interactionClass, theInteraction, tag, null, null);
    }

    @SneakyThrows
    public void receiveInteraction(int interactionClass, ReceivedInteraction theInteraction,
                                   byte[] tag, LogicalTime theTime,
                                   EventRetractionHandle eventRetractionHandle) {
        if (interactionClass == addCustomerHandle) {
            val id = EncodingHelpers.decodeInt(theInteraction.getValue(0));
            val time = convertTime(theTime);
            externalEvents.add(new ExternalEvent(id, ExternalEvent.EventType.ADD_CUSTOMER_TO_CAR_WASH_QUEUE, time));
            log("Dodano nowego klienta do myjni o id: " + id);
        } else {
            val time = convertTime(theTime);
            externalEvents.add(new ExternalEvent(ExternalEvent.EventType.DELETE_CUSTOMER_FROM_CAR_WASH_QUEUE, time));
            log("Usunieto z myjni klienta");
        }
    }
}
