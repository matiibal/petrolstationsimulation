package org.example.dao;

import lombok.Data;

import java.util.Comparator;

@Data
public class ExternalEvent {

    private int customerId;
    private EventType eventType;
    private Double time;
    private double waitingTime;


    public ExternalEvent(int customerId, EventType eventType, Double time) {
        this.customerId = customerId;
        this.eventType = eventType;
        this.time = time;
    }

    public ExternalEvent(double waitingTime, EventType eventType, Double time) {
        this.waitingTime = waitingTime;
        this.eventType = eventType;
        this.time = time;
    }

    public ExternalEvent(EventType eventType, Double time) {
        this.eventType = eventType;
        this.time = time;
    }

    public enum EventType {
        ADD_CUSTOMER_TO_DISTRIBUTOR_QUEUE, DELETE_CUSTOMER_FROM_DISTRIBUTOR_QUEUE,
        ADD_CUSTOMER_TO_CAR_WASH_QUEUE, DELETE_CUSTOMER_FROM_CAR_WASH_QUEUE,
        ADD_CUSTOMER_TO_CASH_DESK_QUEUE, DELETE_CUSTOMER_FROM_CASH_DESK_QUEUE,
        TIME_WAIT_TO_DISTRIBUTOR, TIME_WAIT_TO_CAR_WASH, TIME_WAIT_TO_CASH_DESK
    }

    public static class ExternalEventComparator implements Comparator<ExternalEvent> {
        @Override
        public int compare(ExternalEvent o1, ExternalEvent o2) {
            return o1.time.compareTo(o2.time);
        }
    }
}
