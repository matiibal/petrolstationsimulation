package org.example.dao;

import lombok.Data;

@Data
public class CashDesk {

    private int id;
    private boolean isAvailable;
    private double serviceTime;

    public CashDesk(int id, boolean isAvailable) {
        this.id = id;
        this.isAvailable = isAvailable;
    }


}
