package org.example.dao;

import lombok.Data;
import lombok.val;

import java.util.Random;

@Data
public class Customer {
    private int id;
    private double addTime;
    private double removeTime;
    private double waitingTime;
    private double amountOfFuel;
    private boolean isGoToWashCar;
    public Customer(int id, double addTime) {
        this.id = id;
        this.addTime = addTime;
        this.waitingTime = 0d;
        this.amountOfFuel = generateAmountOfFuel(1, 100);
    }

    public void setRemoveTime(double removeTime) {
        this.removeTime = removeTime;
    }

    public void calculateWaitingTime() {
        waitingTime = removeTime - addTime;
    }

    public double generateAmountOfFuel(double min, double max) {
        val random = new Random();
        return random.nextDouble() * max + min;
    }
}
