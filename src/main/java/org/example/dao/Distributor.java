package org.example.dao;

import lombok.Data;

@Data
public class Distributor {

    private int id;
    private boolean isAvailable;
    private double serviceTime;

    public Distributor(int id, boolean isAvailable) {
        this.id = id;
        this.isAvailable = isAvailable;
    }

}
