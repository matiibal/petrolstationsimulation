package org.example.dao;

import lombok.Data;

@Data
public class CarWash {

    private int id;
    private boolean isAvailable;
    private double serviceTime;

    public CarWash(int id, boolean isAvailable) {
        this.id = id;
        this.isAvailable = isAvailable;
    }


}
