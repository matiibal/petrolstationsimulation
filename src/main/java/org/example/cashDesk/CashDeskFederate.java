package org.example.cashDesk;

import hla.rti.*;
import hla.rti.jlc.RtiFactoryFactory;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.example.dao.CashDesk;
import org.example.dao.ExternalEvent;
import org.portico.impl.hla13.types.DoubleTime;
import org.portico.impl.hla13.types.DoubleTimeInterval;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import static org.example.util.PorticoUtil.convertInterval;
import static org.example.util.PorticoUtil.convertTime;

public class CashDeskFederate {

    private final int numberOfCarWash = 1;

    public static final String READY_TO_RUN = "ReadyToRun";
    private final double timeStep = 1.0;
    private RTIambassador rtIambassador;
    private CashDeskAmbassador cashDeskAmbassador;
    private List<CashDesk> cashDeskList;

    @SneakyThrows
    public static void main(String[] args) {
        new CashDeskFederate().runFederate();
    }

    public void runFederate() throws RTIexception {
        rtIambassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
        try {
            val fom = new File("station.fed");
            rtIambassador.createFederationExecution("Federation - Station",
                    fom.toURI().toURL());
            log("Created Federation");
        } catch (FederationExecutionAlreadyExists exists) {
            log("Didn't create federation, it already existed");
        } catch (MalformedURLException urle) {
            log("Exception processing fom: " + urle.getMessage());
            urle.printStackTrace();
            return;
        }

        cashDeskAmbassador = new CashDeskAmbassador(this);
        rtIambassador.joinFederationExecution("CashDeskFederation", "Federation - Station", cashDeskAmbassador);
        log("Joined Federation as CarWashFederation");

        rtIambassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);

        while (!cashDeskAmbassador.isAnnounced) rtIambassador.tick();

        waitForUser();

        rtIambassador.synchronizationPointAchieved(READY_TO_RUN);
        log("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");

        while (!cashDeskAmbassador.isReadyToRun) rtIambassador.tick();
        enableTimePolicy();
        createCarWash(numberOfCarWash);
        publishAndSubscribe();

        while (cashDeskAmbassador.running) {
            val timeToAdvance = cashDeskAmbassador.federateTime + timeStep;

            advanceTime(1.0);
            this.waitOnCustomer(cashDeskAmbassador.federateTime);
            if (cashDeskAmbassador.externalEvents.size() > 0) {
                cashDeskAmbassador.externalEvents.sort(new ExternalEvent.ExternalEventComparator());
                for (ExternalEvent externalEvent : cashDeskAmbassador.externalEvents) {
                    cashDeskAmbassador.federateTime = externalEvent.getTime();
                    switch (externalEvent.getEventType()) {
//                        case DELETE_CUSTOMER_FROM_CASH_DESK_QUEUE:
//                            getCustomerFromQueue(externalEvent.getCustomerId(), cashDeskAmbassador.federateTime + cashDeskAmbassador.federateLookahead);
//                            break;
                        case ADD_CUSTOMER_TO_CASH_DESK_QUEUE:
                            getCustomerFromQueue(externalEvent.getCustomerId(), cashDeskAmbassador.federateTime + cashDeskAmbassador.federateLookahead);
                            break;

                    }
                }
                cashDeskAmbassador.externalEvents.clear();
            }
            rtIambassador.tick();
        }
    }


    private void waitOnCustomer(double theTime) {
        for (CashDesk cashDesk : cashDeskList) {
            if (!cashDesk.isAvailable()) {
                if (cashDesk.getServiceTime() <= cashDeskAmbassador.federateTime) {
                    cashDesk.setAvailable(true);
                    cashDesk.setServiceTime(-1.0);
                    cashDeskAmbassador.externalEvents.add(new ExternalEvent(cashDeskAmbassador.queueSize, ExternalEvent.EventType.DELETE_CUSTOMER_FROM_CASH_DESK_QUEUE, theTime));
                    log("Koniec obslugi klienta w kasie: " + theTime);
                }
            }
        }
    }


    private void createCarWash(int numberOfCashDesk) {
        cashDeskList = new ArrayList<>(numberOfCashDesk);
        for (int i = 0; i < numberOfCashDesk; i++) {
            CashDesk cashDesk = new CashDesk(i, true);
            cashDeskList.add(cashDesk);
        }
    }

    private void getCustomerFromQueue(int queueSize, double timeStep) throws RTIexception {
        for (int i = 0; i < queueSize && i < numberOfCarWash; i++) {
            for (CashDesk cashDesk : cashDeskList) {
                if (cashDesk.isAvailable()) {
                    cashDesk.setAvailable(false);
                    cashDesk.setServiceTime(cashDeskAmbassador.federateTime + this.randomServiceTime());
                    sendGetUserInterAction(timeStep);
                    log("Rozpoczęto obslugę klienta przy kasie");
                    return;
                }
            }
        }
    }

    public double randomServiceTime() {
        return ((Math.random() * (40 - 2)) + 2);
    }

    private void sendGetUserInterAction(double timeStep) throws RTIexception {
        val parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
        val interactionHandle = rtIambassador.getInteractionClassHandle("InteractionRoot.GetCustomerFromCashDeskQueue");
        val time = convertTime(timeStep);
        rtIambassador.sendInteraction(interactionHandle, parameters, "tag".getBytes(), time);
    }

    private void waitForUser() {
        log(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        val reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            log("Error while waiting for user input: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void enableTimePolicy() throws RTIexception {
        val currentTime = convertTime(cashDeskAmbassador.federateTime);
        val lookahead = convertInterval(cashDeskAmbassador.federateLookahead);

        this.rtIambassador.enableTimeRegulation(currentTime, lookahead);
        while (!cashDeskAmbassador.isRegulating) rtIambassador.tick();

        this.rtIambassador.enableTimeConstrained();
        while (!cashDeskAmbassador.isConstrained) rtIambassador.tick();
    }

    private void publishAndSubscribe() throws RTIexception {
        var classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.QueueCashDesk");
        var queueHandle = rtIambassador.getAttributeHandle("size", classHandle);
        val attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(queueHandle);
        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);

        val addCarWashHandle = rtIambassador.getInteractionClassHandle("InteractionRoot.GetCustomerFromCashDeskQueue");
        rtIambassador.publishInteractionClass(addCarWashHandle);

    }

    private void advanceTime(double timeStep) throws RTIexception {
        cashDeskAmbassador.isAdvancing = true;
        val newTime = convertTime(cashDeskAmbassador.federateTime + timeStep);
        rtIambassador.timeAdvanceRequest(newTime);
        while (cashDeskAmbassador.isAdvancing) rtIambassador.tick();
    }


    private void log(String message) {
        System.out.println("CashDeskFederate: " + message);
    }

}
