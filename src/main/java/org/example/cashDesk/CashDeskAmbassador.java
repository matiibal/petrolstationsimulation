package org.example.cashDesk;


import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;
import hla.rti.ReflectedAttributes;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.NullFederateAmbassador;
import lombok.SneakyThrows;
import lombok.val;
import org.example.customer.CustomerFederate;
import org.example.dao.ExternalEvent;
import org.portico.impl.hla13.types.DoubleTime;

import java.util.ArrayList;

import static org.example.util.PorticoUtil.convertTimeAmbassador;

public class CashDeskAmbassador extends NullFederateAmbassador {

    protected double federateTime = 0.0;

    protected double federateLookahead = 1.0;
    protected boolean isRegulating = false;
    protected boolean isConstrained = false;
    protected boolean isAdvancing = false;
    protected boolean isAnnounced = false;
    protected boolean isReadyToRun = false;
    protected boolean running = true;
    protected int queueSize;

    protected ArrayList<ExternalEvent> externalEvents = new ArrayList<>();
    private final CashDeskFederate fed;

    public CashDeskAmbassador(CashDeskFederate fed) {
        this.fed = fed;
    }

    private void log(String message) {
        System.out.println("CashDeskAmbassador: " + message);
    }

    public void synchronizationPointRegistrationFailed(String label) {
        log("Failed to register sync point: " + label);
    }

    public void synchronizationPointRegistrationSucceeded(String label) {
        log("Successfully registered sync point: " + label);
    }

    public void announceSynchronizationPoint(String label, byte[] tag) {
        log("Synchronization point announced: " + label);
        if (label.equals(CustomerFederate.READY_TO_RUN))
            this.isAnnounced = true;
    }

    public void federationSynchronized(String label) {
        log("Federation Synchronized: " + label);
        if (label.equals(CustomerFederate.READY_TO_RUN))
            this.isReadyToRun = true;
    }

    public void timeRegulationEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTimeAmbassador(theFederateTime);
        this.isRegulating = true;
    }

    public void timeConstrainedEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTimeAmbassador(theFederateTime);
        this.isConstrained = true;
    }

    public void timeAdvanceGrant(LogicalTime theTime) {
        this.federateTime = convertTimeAmbassador(theTime);
        this.isAdvancing = false;
    }

    public void reflectAttributeValues(int theObject, ReflectedAttributes theAttributes, byte[] tag) {
        reflectAttributeValues(theObject, theAttributes, tag, null, null);
    }


    @SneakyThrows
    public void reflectAttributeValues(int theObject, ReflectedAttributes theAttributes,
                                       byte[] tag, LogicalTime theTime, EventRetractionHandle retractionHandle) {
        for (int i = 0; i < theAttributes.size(); i++) {
                queueSize = EncodingHelpers.decodeInt(theAttributes.getValue(i));
                val time = convertTimeAmbassador(theTime);
                externalEvents.add(new ExternalEvent(queueSize, ExternalEvent.EventType.ADD_CUSTOMER_TO_CASH_DESK_QUEUE, time));
                log("Kolejka do kasy ma rozmiar: " + queueSize);
        }
    }

}
