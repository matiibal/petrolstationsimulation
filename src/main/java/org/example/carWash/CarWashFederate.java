package org.example.carWash;

import hla.rti.FederationExecutionAlreadyExists;
import hla.rti.LogicalTimeInterval;
import hla.rti.RTIambassador;
import hla.rti.RTIexception;
import hla.rti.jlc.RtiFactoryFactory;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.example.dao.CarWash;
import org.example.dao.ExternalEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.example.util.PorticoUtil.convertInterval;
import static org.example.util.PorticoUtil.convertTime;

public class CarWashFederate {

    private final int numberOfCarWash = 1;

    public static final String READY_TO_RUN = "ReadyToRun";
    private final double timeStep = 1.0;
    private RTIambassador rtIambassador;
    private CarWashAmbassador carWashAmbassador;
    private List<CarWash> carWashList;

    @SneakyThrows
    public static void main(String[] args) {
        new CarWashFederate().runFederate();
    }

    public void runFederate() throws RTIexception {
        rtIambassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
        try {
            val fom = new File("station.fed");
            rtIambassador.createFederationExecution("Federation - Station",
                    fom.toURI().toURL());
            log("Created Federation");
        } catch (FederationExecutionAlreadyExists exists) {
            log("Didn't create federation, it already existed");
        } catch (MalformedURLException urle) {
            log("Exception processing fom: " + urle.getMessage());
            urle.printStackTrace();
            return;
        }

        carWashAmbassador = new CarWashAmbassador(this);
        rtIambassador.joinFederationExecution("CarWashFederation", "Federation - Station", carWashAmbassador);
        log("Joined Federation as CarWashFederation");

        rtIambassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);

        while (!carWashAmbassador.isAnnounced) rtIambassador.tick();

        waitForUser();

        rtIambassador.synchronizationPointAchieved(READY_TO_RUN);
        log("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");

        while (!carWashAmbassador.isReadyToRun) rtIambassador.tick();
        enableTimePolicy();
        createCarWash(numberOfCarWash);
        publishAndSubscribe();

        while (carWashAmbassador.running) {
            val timeToAdvance = carWashAmbassador.federateTime + timeStep;

            advanceTime(1.0);
            this.waitOnCustomer(carWashAmbassador.federateTime);
            if (carWashAmbassador.externalEvents.size() > 0) {
                carWashAmbassador.externalEvents.sort(new ExternalEvent.ExternalEventComparator());
                for (ExternalEvent externalEvent : carWashAmbassador.externalEvents) {
                    carWashAmbassador.federateTime = externalEvent.getTime();
                    switch (externalEvent.getEventType()) {
                        case ADD_CUSTOMER_TO_CAR_WASH_QUEUE:
                            getCustomerFromQueue(externalEvent.getCustomerId(), carWashAmbassador.federateTime + carWashAmbassador.federateLookahead);
                            break;


                    }
                }
                carWashAmbassador.externalEvents.clear();
            }
            rtIambassador.tick();
        }
    }


    private void waitOnCustomer(double theTime) {
        for (CarWash carWash : carWashList) {
            if (!carWash.isAvailable()) {
                if (carWash.getServiceTime() <= carWashAmbassador.federateTime) {
                    carWash.setAvailable(true);
                    carWash.setServiceTime(-1.0);
                    carWashAmbassador.externalEvents.add(new ExternalEvent(carWashAmbassador.queueSize, ExternalEvent.EventType.DELETE_CUSTOMER_FROM_DISTRIBUTOR_QUEUE, theTime));
                    log("Koniec obslugi klienta w myjni: " + theTime);
                }
            }
        }
    }


    private void createCarWash(int numberOfCarWash) {
        carWashList = new ArrayList<>(numberOfCarWash);
        for (int i = 0; i < numberOfCarWash; i++) {
            CarWash carWash = new CarWash(i, true);
            carWashList.add(carWash);
        }
    }

    private void getCustomerFromQueue(int queueSize, double timeStep) throws RTIexception {
        for (int i = 0; i < queueSize && i < numberOfCarWash; i++) {
            for (CarWash carWash : carWashList) {
                if (carWash.isAvailable()) {
                    carWash.setAvailable(false);
                    carWash.setServiceTime(carWashAmbassador.federateTime + this.randomServiceTime());
                    sendGetUserInterAction(timeStep);
                    log("Rozpoczęto obslugę");
                    return;
                }
            }
        }
    }

    public double randomServiceTime() {
        val random = new Random();
        val program = randomProgram();
        var programTime = 30;
        var programName = "rozszerzony";
        if (program==1){
            programTime=30;
            programName="postawowy";
        }
        else if (program==2){
            programTime=50;
            programName="standardowy";
        }
        else if (program==3){
            programTime=70;
            programName="rozszerzony";
        }
        val clientSpeed = 1 + (1.05 - 1) * random.nextDouble();
        val timeService = clientSpeed * programTime;
        log("Klient wybrał program " + programName + " trwać on będzie "+ timeService);
        return timeService;
    }

    public int randomProgram() {
        return (int) ((Math.random() * (3 - 1)) + 1);
    }

    private void sendGetUserInterAction(double timeStep) throws RTIexception {
        val parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
        val interactionHandle = rtIambassador.getInteractionClassHandle("InteractionRoot.GetCustomerFromCarWashQueue");
        val time = convertTime(timeStep);
        rtIambassador.sendInteraction(interactionHandle, parameters, "tag".getBytes(), time);
    }

    private void waitForUser() {
        log(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        val reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            log("Error while waiting for user input: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void enableTimePolicy() throws RTIexception {
        val currentTime = convertTime(carWashAmbassador.federateTime);
        LogicalTimeInterval lookahead = convertInterval(carWashAmbassador.federateLookahead);

        this.rtIambassador.enableTimeRegulation(currentTime, lookahead);
        while (!carWashAmbassador.isRegulating) rtIambassador.tick();

        this.rtIambassador.enableTimeConstrained();
        while (!carWashAmbassador.isConstrained) rtIambassador.tick();
    }

    private void publishAndSubscribe() throws RTIexception {
        var classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.QueueCarWash");
        var queueHandle = rtIambassador.getAttributeHandle("size", classHandle);
        val attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(queueHandle);
        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);

        val addCarWashHandle = rtIambassador.getInteractionClassHandle("InteractionRoot.GetCustomerFromCarWashQueue");
        rtIambassador.publishInteractionClass(addCarWashHandle);

    }

    private void advanceTime(double timeStep) throws RTIexception {
        carWashAmbassador.isAdvancing = true;
        val newTime = convertTime(carWashAmbassador.federateTime + timeStep);
        rtIambassador.timeAdvanceRequest(newTime);
        while (carWashAmbassador.isAdvancing) rtIambassador.tick();
    }

    private void log(String message) {
        System.out.println("CarWashFederate: " + message);
    }

}
