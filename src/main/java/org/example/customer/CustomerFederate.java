package org.example.customer;

import hla.rti.FederationExecutionAlreadyExists;
import hla.rti.RTIambassador;
import hla.rti.RTIexception;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Random;

import static org.example.util.PorticoUtil.convertInterval;
import static org.example.util.PorticoUtil.convertTime;

public class CustomerFederate {

    public static final String READY_TO_RUN = "ReadyToRun";

    private RTIambassador rtIambassador;
    private CustomerAmbassador customerAmbassador;
    private int customerId;

    @SneakyThrows
    public static void main(String[] args) {
        new CustomerFederate().runFederate();
    }

    public void runFederate() throws RTIexception {
        rtIambassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();

        try {
            val fom = new File("station.fed");
            rtIambassador.createFederationExecution("Federation - Station",
                    fom.toURI().toURL());
            log("Created Federation");
        } catch (FederationExecutionAlreadyExists exists) {
            log("Didn't create federation, it already existed");
        } catch (MalformedURLException urle) {
            log("Exception processing fom: " + urle.getMessage());
            urle.printStackTrace();
            return;
        }

        customerAmbassador = new CustomerAmbassador(this);
        rtIambassador.joinFederationExecution("CustomerFederate", "Federation - Station", customerAmbassador);
        log("Joined Federation as org.example.customer.CustomerFederate");

        rtIambassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);

        while (!customerAmbassador.isAnnounced) rtIambassador.tick();

        waitForUser();

        rtIambassador.synchronizationPointAchieved(READY_TO_RUN);
        log("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");

        while (!customerAmbassador.isReadyToRun) rtIambassador.tick();
        enableTimePolicy();

        publishAndSubscribe();

        while (customerAmbassador.running) {
            createClient(customerAmbassador.federateTime + customerAmbassador.federateLookahead);
            advanceTime(randomTime());
            rtIambassador.tick();
        }
    }

    private void waitForUser() {
        log(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            log("Error while waiting for user input: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void enableTimePolicy() throws RTIexception {
        val currentTime = convertTime(customerAmbassador.federateTime);
        val lookahead = convertInterval(customerAmbassador.federateLookahead);

        this.rtIambassador.enableTimeRegulation(currentTime, lookahead);
        while (!customerAmbassador.isRegulating) rtIambassador.tick();

        this.rtIambassador.enableTimeConstrained();
        while (!customerAmbassador.isConstrained) rtIambassador.tick();
    }

    private void publishAndSubscribe() throws RTIexception {
        val addCustomerHandle = rtIambassador.getInteractionClassHandle("InteractionRoot.AddCustomerToDistributorQueue");
        rtIambassador.publishInteractionClass(addCustomerHandle);
    }

    private void advanceTime(double timeStep) throws RTIexception {
        customerAmbassador.isAdvancing = true;
        val newTime = convertTime(customerAmbassador.federateTime + timeStep);
        rtIambassador.timeAdvanceRequest(newTime);
        while (customerAmbassador.isAdvancing) rtIambassador.tick();
    }

    private double randomTime() {
        val random = new Random();
        return 15 + (9 * random.nextDouble());
    }

    private void createClient(double timeStep) throws RTIexception {
        val parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
        customerId++;
        var idCustomer = EncodingHelpers.encodeInt(customerId);
        var interactionHandle = rtIambassador.getInteractionClassHandle("InteractionRoot.AddCustomerToDistributorQueue");
        var IdHandle = rtIambassador.getParameterHandle("idCustomer", interactionHandle);
        parameters.add(IdHandle, idCustomer);
        val time = convertTime(timeStep);
        rtIambassador.sendInteraction(interactionHandle, parameters, "tag".getBytes(), time);
        log("Dodano nowego kilenta, id: " + customerId + " CZAS SYMULACJI: " + customerAmbassador.federateTime);
    }


    private void log(String message) {
        System.out.println("CustomerFederate: " + message);
    }

}
