package org.example.distributor;

import hla.rti.*;
import hla.rti.jlc.RtiFactoryFactory;
import lombok.val;
import lombok.var;
import org.example.dao.Distributor;
import org.example.dao.ExternalEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.example.util.PorticoUtil.convertInterval;
import static org.example.util.PorticoUtil.convertTime;

public class DistributorFederate {

    private final int numberOfDistributor = 9;

    public static final String READY_TO_RUN = "ReadyToRun";
    private final double timeStep = 1.0;
    private RTIambassador rtIambassador;
    private DistributorAmbassador distributorAmbassador;
    private List<Distributor> distributorList;

    public static void main(String[] args) {
        try {
            new DistributorFederate().runFederate();
        } catch (RTIexception rtIexception) {
            rtIexception.printStackTrace();
        }
    }

    public void runFederate() throws RTIexception {
        rtIambassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();

        try {
            val fom = new File("station.fed");
            rtIambassador.createFederationExecution("Federation - Station",
                    fom.toURI().toURL());
            log("Created Federation");
        } catch (FederationExecutionAlreadyExists exists) {
            log("Didn't create federation, it already existed");
        } catch (MalformedURLException urle) {
            log("Exception processing fom: " + urle.getMessage());
            urle.printStackTrace();
            return;
        }

        distributorAmbassador = new DistributorAmbassador(this);
        rtIambassador.joinFederationExecution("DistributorFederation", "Federation - Station", distributorAmbassador);
        log("Joined Federation as DistributorFederation");

        rtIambassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);

        while (!distributorAmbassador.isAnnounced) rtIambassador.tick();

        waitForUser();

        rtIambassador.synchronizationPointAchieved(READY_TO_RUN);
        log("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");

        while (!distributorAmbassador.isReadyToRun) rtIambassador.tick();
        enableTimePolicy();
        createDistributor(numberOfDistributor);
        publishAndSubscribe();

        while (distributorAmbassador.running) {
            double timeToAdvance = distributorAmbassador.federateTime + timeStep;
            advanceTime(1.0);
            waitOnCustomer(distributorAmbassador.federateTime);
            if (distributorAmbassador.externalEvents.size() > 0) {
                distributorAmbassador.externalEvents.sort(new ExternalEvent.ExternalEventComparator());
                for (ExternalEvent externalEvent : distributorAmbassador.externalEvents) {
                    distributorAmbassador.federateTime = externalEvent.getTime();
                    switch (externalEvent.getEventType()) {
                        case DELETE_CUSTOMER_FROM_DISTRIBUTOR_QUEUE:
                            getCustomerFromQueue(externalEvent.getCustomerId(), distributorAmbassador.federateTime + distributorAmbassador.federateLookahead);
                            break;
                    }
                }
                distributorAmbassador.externalEvents.clear();
            }
            rtIambassador.tick();
        }
    }


    private void waitOnCustomer(double theTime) {
        for (Distributor distributor : distributorList) {
            if (!distributor.isAvailable()) {
                if (distributor.getServiceTime() <= distributorAmbassador.federateTime) {
                    distributor.setAvailable(true);
                    distributor.setServiceTime(-1.0);
                    distributorAmbassador.externalEvents.add(new ExternalEvent(distributorAmbassador.queueSize, ExternalEvent.EventType.DELETE_CUSTOMER_FROM_DISTRIBUTOR_QUEUE, theTime));
                    log("Koniec obslugi klienta przy dystrybutorze, CZAS SYMULACJI:" + theTime);
                }
            }
        }
    }

    private void createDistributor(int numberOfDistributor) {
        distributorList = new ArrayList<>(numberOfDistributor);
        for (int i = 0; i < numberOfDistributor; i++) {
            Distributor distributor = new Distributor(i, true);
            distributorList.add(distributor);
        }
    }

    private void getCustomerFromQueue(int queueSize, double timeStep) throws RTIexception {
        for (int i = 0; i < queueSize && i < numberOfDistributor; i++) {
            for (Distributor distributor : distributorList) {
                if (distributor.isAvailable()) {
                    distributor.setAvailable(false);
                    distributor.setServiceTime(distributorAmbassador.federateTime + randomServiceTime());
                    sendGetUserInterAction(timeStep);
                    log("Rozpoczęto obslugę");
                    return;
                }
            }
        }
    }

    public double randomServiceTime() {
        val random = new Random();
        val fuel = randomFuel(random);
        val clientSpeed = 1 + (1.05 - 1) * random.nextDouble();
        val timeService = clientSpeed * fuel;
        log("Klient zatakował " + String.format("%.2f", fuel) + "l paliwa w czasie " + String.format("%.2f", timeService));
        return clientSpeed * fuel;
    }

    public double randomFuel(Random random) {
        return 5 + 299 * random.nextDouble();
    }


    private void sendGetUserInterAction(double timeStep) throws RTIexception {
        val parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
        int interactionHandle = rtIambassador.getInteractionClassHandle("InteractionRoot.GetCustomer");
        val time = convertTime(timeStep);
        rtIambassador.sendInteraction(interactionHandle, parameters, "tag".getBytes(), time);
    }

    private void waitForUser() {
        log(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        val reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            log("Error while waiting for user input: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void enableTimePolicy() throws RTIexception {
        val currentTime = convertTime(distributorAmbassador.federateTime);
        LogicalTimeInterval lookahead = convertInterval(distributorAmbassador.federateLookahead);

        this.rtIambassador.enableTimeRegulation(currentTime, lookahead);
        while (!distributorAmbassador.isRegulating) rtIambassador.tick();

        this.rtIambassador.enableTimeConstrained();
        while (!distributorAmbassador.isConstrained) rtIambassador.tick();
    }

    private void publishAndSubscribe() throws RTIexception {

        var classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.QueueDistributor");
        var queueHandle = rtIambassador.getAttributeHandle("size", classHandle);

        val attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(queueHandle);

        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);

        val getCustomerHandle = rtIambassador.getInteractionClassHandle("InteractionRoot.GetCustomer");
        rtIambassador.publishInteractionClass(getCustomerHandle);

    }

    private void advanceTime(double timestep) throws RTIexception {
        distributorAmbassador.isAdvancing = true;
        LogicalTime newTime = convertTime(distributorAmbassador.federateTime + timestep);
        rtIambassador.timeAdvanceRequest(newTime);
        while (distributorAmbassador.isAdvancing) rtIambassador.tick();
    }

    private void log(String message) {
        System.out.println("DistributorFederate: " + message);
    }

}
