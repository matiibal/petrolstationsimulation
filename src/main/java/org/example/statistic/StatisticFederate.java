package org.example.statistic;

import hla.rti.FederationExecutionAlreadyExists;
import hla.rti.RTIambassador;
import hla.rti.RTIexception;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.example.dao.ExternalEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import static org.example.util.PorticoUtil.convertInterval;
import static org.example.util.PorticoUtil.convertTime;

public class StatisticFederate {

    public static final String READY_TO_RUN = "ReadyToRun";
    protected int timeHlaHandle = 0;
    protected int queueHlaHandle = 0;
    protected int statisticHandle = 0;
    private RTIambassador rtIambassador;
    private StatisticAmbassador statisticAmbassador;

    private double waitingTimeToDistributor = 0.0;
    private double avgWaitingTimeToDistributor = 0.0;
    private int countWaitingPeopleToDistributor = 1;
    private int queueSizeToDistributor = 0;

    private double waitingTimeToCarWash = 0.0;
    private double avgWaitingTimeToCarWash = 0.0;
    private int countWaitingPeopleToCarWash = 1;
    private int queueSizeToCarWash = 0;

    private double waitingTimeToCashDesk = 0.0;
    private double avgWaitingTimeToCashDesk = 0.0;
    private int countWaitingPeopleToCashDesk = 1;
    private int queueSizeToCashDesk = 0;


    @SneakyThrows
    public static void main(String[] args) {
        new StatisticFederate().runFederate();
    }

    public void runFederate() throws Exception {
        rtIambassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
        try {
            val fom = new File("station.fed");
            rtIambassador.createFederationExecution("Federation - Station",
                    fom.toURI().toURL());
            log("Created Federation");
        } catch (FederationExecutionAlreadyExists exists) {
            log("Didn't create federation, it already existed");
        } catch (MalformedURLException urle) {
            log("Exception processing fom: " + urle.getMessage());
            urle.printStackTrace();
            return;
        }
        statisticAmbassador = new StatisticAmbassador(this);
        rtIambassador.joinFederationExecution("StatisticFederate", "Federation - Station", statisticAmbassador);
        log("Joined Federation as " + "StatisticFederate");

        rtIambassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);

        while (!statisticAmbassador.isAnnounced) rtIambassador.tick();
        waitForUser();

        rtIambassador.synchronizationPointAchieved(READY_TO_RUN);
        log("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");
        while (!statisticAmbassador.isReadyToRun) rtIambassador.tick();

        enableTimePolicy();

        publishAndSubscribe();
        this.registerStorageObject();
        log(statisticAmbassador.federateTime + " Published and Subscribed");

        while (statisticAmbassador.running) {
            if (statisticAmbassador.externalEvents.size() > 0) {
                statisticAmbassador.externalEvents.sort(new ExternalEvent.ExternalEventComparator());
                for (val externalEvent : statisticAmbassador.externalEvents) {
                    statisticAmbassador.federateTime = externalEvent.getTime();
                    switch (externalEvent.getEventType()) {
                        case ADD_CUSTOMER_TO_DISTRIBUTOR_QUEUE:
                            updateDistributorQueueSize(externalEvent.getCustomerId(), statisticAmbassador.federateTime + statisticAmbassador.federateLookahead);
                            break;
                        case TIME_WAIT_TO_DISTRIBUTOR:
                            updateWaitingTimeToDistributor(externalEvent.getWaitingTime(), statisticAmbassador.federateTime + statisticAmbassador.federateLookahead);
                            break;
                        case ADD_CUSTOMER_TO_CAR_WASH_QUEUE:
                            updateCarWashQueueSize(externalEvent.getCustomerId(), statisticAmbassador.federateTime + statisticAmbassador.federateLookahead);
                            break;
                        case TIME_WAIT_TO_CAR_WASH:
                            updateWaitingTimeToCarWash(externalEvent.getWaitingTime(), statisticAmbassador.federateTime + statisticAmbassador.federateLookahead);
                            break;
                        case ADD_CUSTOMER_TO_CASH_DESK_QUEUE:
                            updateCashDeskQueueSize(externalEvent.getCustomerId(), statisticAmbassador.federateTime + statisticAmbassador.federateLookahead);
                            break;
                        case TIME_WAIT_TO_CASH_DESK:
                            updateWaitingTimeToCashDesk(externalEvent.getWaitingTime(), statisticAmbassador.federateTime + statisticAmbassador.federateLookahead);
                            break;
                    }
                }
                statisticAmbassador.externalEvents.clear();
            }
            rtIambassador.tick();
            advanceTime(1.0);
        }
    }

    private void updateWaitingTimeToCarWash(double waitingTime, double time) throws RTIexception {
        this.waitingTimeToCarWash = this.waitingTimeToCarWash + waitingTime;
        this.avgWaitingTimeToCarWash = this.waitingTimeToCarWash / countWaitingPeopleToCarWash;
        countWaitingPeopleToCarWash++;
        log("Średni czas oczekiwania do myjni wynosi: " + this.avgWaitingTimeToCarWash);
        updateGuiStatistics(time);
    }
    
    private void updateWaitingTimeToDistributor(double waitingTime, double time) throws RTIexception {
        this.waitingTimeToDistributor = this.waitingTimeToDistributor + waitingTime;
        this.avgWaitingTimeToDistributor = this.waitingTimeToDistributor / countWaitingPeopleToDistributor;
        countWaitingPeopleToDistributor++;
        log("Średni czas oczekiwania do dystrybutora wynosi: " + this.avgWaitingTimeToDistributor);
        updateGuiStatistics(time);
    }

    private void updateDistributorQueueSize(int queueSizeToDistributor, double time) throws RTIexception {
        this.queueSizeToDistributor = queueSizeToDistributor;
        updateGuiStatistics(time);
    }

    private void updateCarWashQueueSize(int queueSizeToCarWash, double time) throws RTIexception {
        this.queueSizeToCarWash = queueSizeToCarWash;
        updateGuiStatistics(time);
    }

    private void updateCashDeskQueueSize(int queueSizeToCashDesk, double time) throws RTIexception {
        this.queueSizeToCashDesk = queueSizeToCashDesk;
        updateGuiStatistics(time);
    }

    private void updateWaitingTimeToCashDesk(double waitingTime, double time) throws RTIexception {
        this.waitingTimeToCashDesk = this.waitingTimeToCashDesk + waitingTime;
        this.avgWaitingTimeToCashDesk = this.waitingTimeToCashDesk / countWaitingPeopleToCashDesk;
        countWaitingPeopleToCashDesk++;
        log("Średni czas oczekiwania do kasy wynosi: " + this.avgWaitingTimeToCashDesk);
        updateGuiStatistics(time);
    }

    private void updateGuiStatistics(double time) throws RTIexception {
        val attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();

        val classHandle = rtIambassador.getObjectClass(statisticHandle);
        val sizeDistributorHandle = rtIambassador.getAttributeHandle("queueSizeToDistributor", classHandle);
        val timeDistributorHandle = rtIambassador.getAttributeHandle("waitingTimeToDistributor", classHandle);
        val sizeCarWashHandle = rtIambassador.getAttributeHandle("queueSizeToCarWash", classHandle);
        val sizeCashDeskHandle = rtIambassador.getAttributeHandle("queueSizeToCashDesk", classHandle);
        val queueDistributorSizeBytes = EncodingHelpers.encodeInt(this.queueSizeToDistributor);
        val waitingDistributorTimeBytes = EncodingHelpers.encodeDouble(this.avgWaitingTimeToDistributor);
        val queueCarWashSizeBytes = EncodingHelpers.encodeInt(this.queueSizeToCarWash);
        val queueCashDeskSizeBytes = EncodingHelpers.encodeInt(this.queueSizeToCashDesk);

        attributes.add(sizeDistributorHandle, queueDistributorSizeBytes);
        attributes.add(timeDistributorHandle, waitingDistributorTimeBytes);
        attributes.add(sizeCarWashHandle, queueCarWashSizeBytes);
        attributes.add(sizeCashDeskHandle, queueCashDeskSizeBytes);

        val logicalTime = convertTime(time);
        rtIambassador.updateAttributeValues(statisticHandle, attributes, "tag".getBytes(), logicalTime);
    }


    private void waitForUser() {
        log(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        val reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            log("Error while waiting for user input: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void advanceTime(double timeStep) throws RTIexception {
        statisticAmbassador.isAdvancing = true;
        val newTime = convertTime(statisticAmbassador.federateTime + timeStep);
        rtIambassador.timeAdvanceRequest(newTime);
        while (statisticAmbassador.isAdvancing) rtIambassador.tick();
    }

    private void registerStorageObject() throws RTIexception {
        val classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.Statistics");
        statisticHandle = rtIambassador.registerObjectInstance(classHandle);
    }

    private void publishAndSubscribe() throws RTIexception {
        var classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.Statistics");
        var queueSizeToDistributorHandle = rtIambassador.getAttributeHandle("queueSizeToDistributor", classHandle);
        var waitingTimeToDistributorHandle = rtIambassador.getAttributeHandle("waitingTimeToDistributor", classHandle);
        var queueSizeToCarWashHandle = rtIambassador.getAttributeHandle("queueSizeToCarWash", classHandle);
        var queueSizeToCashDeskHandle = rtIambassador.getAttributeHandle("queueSizeToCashDesk", classHandle);

        var attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(queueSizeToDistributorHandle);
        attributes.add(waitingTimeToDistributorHandle);
        attributes.add(queueSizeToCarWashHandle);
        attributes.add(queueSizeToCashDeskHandle);

        rtIambassador.publishObjectClass(classHandle, attributes);

        classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.QueueDistributor");
        var queueToDistributorHandle = rtIambassador.getAttributeHandle("size", classHandle);

        statisticAmbassador.setQueueToDistributorHandle(queueToDistributorHandle);

        attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(queueToDistributorHandle);
        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);

        classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.QueueCashDesk");
        var queueToCashDeskHandle = rtIambassador.getAttributeHandle("size", classHandle);

        statisticAmbassador.setQueueToCashDeskHandle(queueToCashDeskHandle);

        attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(queueToCashDeskHandle);
        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);

        classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.WaitingTimeToDistributor");
        var timeHandle = rtIambassador.getAttributeHandle("time", classHandle);
        statisticAmbassador.setTimeWaitToDistributorHandle(timeHandle);
        attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(timeHandle);
        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);

        classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.QueueCarWash");
        var queueToCarWashHandle = rtIambassador.getAttributeHandle("size", classHandle);
        statisticAmbassador.setQueueToCarWashHandle(queueToCarWashHandle);


        this.queueHlaHandle = classHandle;
        attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(queueToCarWashHandle);
        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);

        classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.WaitingTimeToCarWash");
        var timeToCarWashHandle = rtIambassador.getAttributeHandle("time", classHandle);

        statisticAmbassador.setTimeWaitToCarWashHandle(timeToCarWashHandle);
        this.timeHlaHandle = classHandle;
        attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(timeToCarWashHandle);
        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);

    }

    private void enableTimePolicy() throws RTIexception {
        val currentTime = convertTime(statisticAmbassador.federateTime);
        val lookahead = convertInterval(statisticAmbassador.federateLookahead);

        this.rtIambassador.enableTimeRegulation(currentTime, lookahead);

        while (!statisticAmbassador.isRegulating) rtIambassador.tick();

        this.rtIambassador.enableTimeConstrained();

        while (!statisticAmbassador.isConstrained) rtIambassador.tick();
    }

    private void log(String message) {
        System.out.println("StatisticsFederate: " + message);
    }

}
