package org.example.statistic;

import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;
import hla.rti.ReceivedInteraction;
import hla.rti.ReflectedAttributes;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.NullFederateAmbassador;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.val;
import org.example.dao.ExternalEvent;
import org.portico.impl.hla13.types.DoubleTime;

import java.util.ArrayList;

import static org.example.util.PorticoUtil.convertTimeAmbassador;

public class StatisticAmbassador extends NullFederateAmbassador {

    protected boolean running = true;

    protected double federateTime = 0.0;
    protected double federateLookahead = 1.0;
    protected boolean isRegulating = false;
    protected boolean isConstrained = false;
    protected boolean isAdvancing = false;

    protected boolean isAnnounced = false;
    protected boolean isReadyToRun = false;

    protected ArrayList<ExternalEvent> externalEvents = new ArrayList<>();

    private @Setter
    int timeWaitToDistributorHandle = -1;
    private @Setter
    int queueToDistributorHandle = -1;
    private @Setter
    int timeWaitToCarWashHandle = -1;
    private @Setter
    int queueToCarWashHandle = -1;
    private @Setter
    int timeWaitToCashDeskHandle = -1;
    private @Setter
    int queueToCashDeskHandle = -1;


    protected int statisticHandle = 0;

    protected StatisticFederate fed;

    public StatisticAmbassador(StatisticFederate fed) {
        this.fed = fed;
    }

    private void log(String message) {
        System.out.println("StatisticsAmbassador: " + message);
    }

    public void synchronizationPointRegistrationFailed(String label) {
        log("Failed to register sync point: " + label);
    }

    public void synchronizationPointRegistrationSucceeded(String label) {
        log("Successfully registered sync point: " + label);
    }

    public void announceSynchronizationPoint(String label, byte[] tag) {
        log("Synchronization point announced: " + label);
        if (label.equals(StatisticFederate.READY_TO_RUN))
            this.isAnnounced = true;
    }

    public void federationSynchronized(String label) {
        log("Federation Synchronized: " + label);
        if (label.equals(StatisticFederate.READY_TO_RUN))
            this.isReadyToRun = true;
    }

    public void timeRegulationEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTimeAmbassador(theFederateTime);
        this.isRegulating = true;
    }

    public void timeConstrainedEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTimeAmbassador(theFederateTime);
        this.isConstrained = true;
    }

    public void timeAdvanceGrant(LogicalTime theTime) {
        this.federateTime = convertTimeAmbassador(theTime);
        this.isAdvancing = false;
    }

    public void receiveInteraction(int interactionClass,
                                   ReceivedInteraction theInteraction,
                                   byte[] tag) {
        receiveInteraction(interactionClass, theInteraction, tag, null, null);
    }

    public void receiveInteraction(int interactionClass, ReceivedInteraction theInteraction,
                                   byte[] tag, LogicalTime theTime,
                                   EventRetractionHandle eventRetractionHandle) {
    }

    public void reflectAttributeValues(int theObject,
                                       ReflectedAttributes theAttributes, byte[] tag) {
        reflectAttributeValues(theObject, theAttributes, tag, null, null);
    }

    @SneakyThrows
    public void reflectAttributeValues(int theObject, ReflectedAttributes theAttributes,
                                       byte[] tag, LogicalTime theTime,
                                       EventRetractionHandle retractionHandle) {
        for (int i = 0; i < theAttributes.size(); i++) {
            val attributeHandle = theAttributes.getAttributeHandle(0);
            if (attributeHandle == timeWaitToDistributorHandle) {
                val time = convertTimeAmbassador(theTime);
                val waitingTime = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                externalEvents.add(new ExternalEvent(waitingTime, ExternalEvent.EventType.TIME_WAIT_TO_DISTRIBUTOR, time));
                log("Aktualny czas oczekiwania do dysytrybutora to\t" + waitingTime);
            } else if (attributeHandle == timeWaitToCarWashHandle) {
                val time = convertTimeAmbassador(theTime);
                val waitingTime = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                externalEvents.add(new ExternalEvent(waitingTime, ExternalEvent.EventType.TIME_WAIT_TO_DISTRIBUTOR, time));
                log("Aktualny czas oczekiwania do myjni to\t" + waitingTime);
            } else if (attributeHandle == queueToDistributorHandle) {
                val queueSize = EncodingHelpers.decodeInt(theAttributes.getValue(i));
                val time = convertTimeAmbassador(theTime);
                externalEvents.add(new ExternalEvent(queueSize, ExternalEvent.EventType.ADD_CUSTOMER_TO_DISTRIBUTOR_QUEUE, time));
                log("Liczba osób oczekujących do dystrybutora to\t" + queueSize + " osoby");
            } else if (attributeHandle == queueToCarWashHandle) {
                val queueSize = EncodingHelpers.decodeInt(theAttributes.getValue(i));
                val time = convertTimeAmbassador(theTime);
                externalEvents.add(new ExternalEvent(queueSize, ExternalEvent.EventType.ADD_CUSTOMER_TO_CAR_WASH_QUEUE, time));
                log("Liczba osób oczekujących do myjni to\t" + queueSize + " osoby");
            } else if (attributeHandle == queueToCashDeskHandle) {
                val queueSize = EncodingHelpers.decodeInt(theAttributes.getValue(i));
                val time = convertTimeAmbassador(theTime);
                externalEvents.add(new ExternalEvent(queueSize, ExternalEvent.EventType.ADD_CUSTOMER_TO_CASH_DESK_QUEUE, time));
                log("Liczba osób oczekujących do kasy to\t" + queueSize + " osoby");
            } else if (attributeHandle == timeWaitToCashDeskHandle) {
                val time = convertTimeAmbassador(theTime);
                val waitingTime = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                externalEvents.add(new ExternalEvent(waitingTime, ExternalEvent.EventType.TIME_WAIT_TO_CASH_DESK, time));
                log("Aktualny czas oczekiwania do kasy to\t" + waitingTime);
            }
        }
    }

}
