package org.example.gui;

import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;
import hla.rti.ReflectedAttributes;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.NullFederateAmbassador;
import lombok.SneakyThrows;
import lombok.val;
import org.portico.impl.hla13.types.DoubleTime;

import static org.example.util.PorticoUtil.convertTimeAmbassador;

public class GUIAmbassador extends NullFederateAmbassador {
    protected double federateTime = 0.0;
    protected double federateLookahead = 1.0;

    protected boolean isRegulating = false;
    protected boolean isConstrained = false;
    protected boolean isAdvancing = false;

    protected boolean isAnnounced = false;
    protected boolean isReadyToRun = false;

    protected boolean running = true;

    protected GUIFederate fed;

    protected int statisticsHandle;

    public GUIAmbassador(GUIFederate fed) {
        this.fed = fed;
    }

    private void log(String message) {
        System.out.println("GUIAmbassador: " + message);
    }

    public void synchronizationPointRegistrationFailed(String label) {
        log("Failed to register sync point: " + label);
    }

    public void synchronizationPointRegistrationSucceeded(String label) {
        log("Successfully registered sync point: " + label);
    }

    public void announceSynchronizationPoint(String label, byte[] tag) {
        log("Synchronization point announced: " + label);
        if (label.equals(GUIFederate.READY_TO_RUN))
            this.isAnnounced = true;
    }

    public void federationSynchronized(String label) {
        log("Federation Synchronized: " + label);
        if (label.equals(GUIFederate.READY_TO_RUN))
            this.isReadyToRun = true;
    }

    public void timeRegulationEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTimeAmbassador(theFederateTime);
        this.isRegulating = true;
    }

    public void timeConstrainedEnabled(LogicalTime theFederateTime) {
        this.federateTime = convertTimeAmbassador(theFederateTime);
        this.isConstrained = true;
    }

    public void timeAdvanceGrant(LogicalTime theTime) {
        this.federateTime = convertTimeAmbassador(theTime);
        this.isAdvancing = false;
    }

    public void discoverObjectInstance(int theObject, int theObjectClass, String objectName) {
        statisticsHandle = theObject;
    }

    public void reflectAttributeValues(int theObject,
                                       ReflectedAttributes theAttributes,
                                       byte[] tag) {
        reflectAttributeValues(theObject, theAttributes, tag, null, null);
    }

    @SneakyThrows
    public void reflectAttributeValues(int theObject, ReflectedAttributes theAttributes,
                                       byte[] tag, LogicalTime theTime,
                                       EventRetractionHandle retractionHandle) {
        val time = convertTimeAmbassador(theTime);
        val queueSizeToDistributor = EncodingHelpers.decodeInt(theAttributes.getValue(0));
        val waitingTimeToDistributor = EncodingHelpers.decodeDouble(theAttributes.getValue(1));
        val queueSizeToCarWash = EncodingHelpers.decodeInt(theAttributes.getValue(2));
        val queueSizeToCashDesk = EncodingHelpers.decodeInt(theAttributes.getValue(3));
        fed.updateGUI(queueSizeToDistributor, waitingTimeToDistributor,
                queueSizeToCarWash,
                queueSizeToCashDesk, time);

    }
}
