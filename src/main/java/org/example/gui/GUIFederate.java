package org.example.gui;

import hla.rti.FederationExecutionAlreadyExists;
import hla.rti.RTIambassador;
import hla.rti.RTIexception;
import hla.rti.jlc.RtiFactoryFactory;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.val;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import static org.example.util.PorticoUtil.convertInterval;
import static org.example.util.PorticoUtil.convertTime;

public class GUIFederate extends Application {

    public static final String READY_TO_RUN = "ReadyToRun";
    public static final String timeToDistributor = "Średni czas oczekiwania do dystrybutora: ";
    public static final String queueSizeToDistributor = "Długość kolejki do dystrybutora: ";
    // public static final String timeToCarWash = "Średni czas oczekiwania do myjni: ";
    public static final String queueSizeToCarWash = "Długość kolejki do myjni: ";
    // public static final String timeToCashDesk = "Średni czas oczekiwania do kasy: ";
    public static final String queueSizeToCashDesk = "Długość kolejki do kasy: ";

    Label avgTimeLabelToDistributor = new Label(timeToDistributor);
    Label avgQueueSizeLabelToDistributor = new Label(queueSizeToDistributor);
    //   Label avgTimeLabelToCarWash = new Label(timeToCarWash);
    Label avgQueueSizeLabelToCarWash = new Label(queueSizeToCarWash);
    //  Label avgTimeLabelToCashDesk = new Label(timeToCashDesk);
    Label avgQueueSizeLabelToCashDesk = new Label(queueSizeToCashDesk);

    NumberAxis xAxisTimeToDistributor = new NumberAxis();
    NumberAxis yAxisTimeToDistributor = new NumberAxis();
    NumberAxis xAxisLengthToDistributor = new NumberAxis();
    NumberAxis yAxisLengthToDistributor = new NumberAxis();
    LineChart<Number, Number> queueToDistributorSizeChart = new LineChart<>(xAxisLengthToDistributor, yAxisLengthToDistributor);
    NumberAxis xAxisLengthToCarWash = new NumberAxis();
    NumberAxis yAxisLengthToCarWash = new NumberAxis();
    NumberAxis xAxisLengthToCashDesk = new NumberAxis();
    NumberAxis yAxisLengthToCashDesk = new NumberAxis();
    LineChart<Number, Number> waitingTimeToDistributorChart = new LineChart<>(xAxisTimeToDistributor, yAxisTimeToDistributor);
    LineChart.Series<Number, Number> timeSeries = new LineChart.Series<>();
    LineChart.Series<Number, Number> queueSizeSeries = new LineChart.Series<>();

    LineChart<Number, Number> queueToCarWashSizeChart = new LineChart<>(xAxisLengthToCarWash, yAxisLengthToCarWash);
    LineChart.Series<Number, Number> queueCarWashSizeSeries = new LineChart.Series<>();
    LineChart<Number, Number> queueToCashDeskChart = new LineChart<>(xAxisLengthToCashDesk, yAxisLengthToCashDesk);
    LineChart.Series<Number, Number> queueCashDeskSeries = new LineChart.Series<>();

    private int lastQueueSizeToDistributor = 0;
    private int lastQueueSizeToCarWash = 0;
    private int lastQueueSizeToCashDesk = 0;
    private double lastWaitingTimeAvgToDistributor = 0;
    private RTIambassador rtIambassador;
    private GUIAmbassador guiAmbassador;


    @SneakyThrows
    public static void main(String[] args) {
        new GUIFederate().runFederate();
    }

    public void runFederate() throws RTIexception {
        rtIambassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
        try {
            val fom = new File("station.fed");
            rtIambassador.createFederationExecution("Federation - Station",
                    fom.toURI().toURL());
            log("Created Federation");
        } catch (FederationExecutionAlreadyExists exists) {
            log("Didn't create federation, it already existed");
        } catch (MalformedURLException urle) {
            log("Exception processing fom: " + urle.getMessage());
            urle.printStackTrace();
            return;
        }

        guiAmbassador = new GUIAmbassador(this);
        rtIambassador.joinFederationExecution("GUIFederate", "Federation - Station", guiAmbassador);
        log("Joined Federation as GuiFederate");

        rtIambassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);

        while (!guiAmbassador.isAnnounced) rtIambassador.tick();

        waitForUser();

        rtIambassador.synchronizationPointAchieved(READY_TO_RUN);
        log("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");

        while (!guiAmbassador.isReadyToRun) rtIambassador.tick();
        enableTimePolicy();

        publishAndSubscribe();

        this.createGui();

        while (guiAmbassador.running) advanceTime(1.0);
    }

    private void createGui() {

        queueToDistributorSizeChart.setTitle(queueSizeToDistributor);
        waitingTimeToDistributorChart.setTitle(timeToDistributor);
        queueToCarWashSizeChart.setTitle(queueSizeToCarWash);
        queueToCashDeskChart.setTitle(queueSizeToCashDesk);

        timeSeries.setName("Sredni czas oczekiwania do dystrybutora");
        queueSizeSeries.setName("Długość kolejki do dystrybutora");
        queueCarWashSizeSeries.setName("Długość kolejki do myjni");
        queueCashDeskSeries.setName("Długość kolejki do kasy");
        Platform.setImplicitExit(false);

        Platform.runLater(() -> {
            val stage = new Stage();
            stage.setTitle("Stadion");
            val layout = new GridPane();
            layout.add(avgTimeLabelToDistributor, 1, 1);
            layout.add(avgQueueSizeLabelToDistributor, 1, 2);
            layout.add(avgQueueSizeLabelToCarWash, 1, 4);
            layout.add(avgQueueSizeLabelToCashDesk, 1, 6);
            layout.add(waitingTimeToDistributorChart, 1, 8);
            layout.add(queueToDistributorSizeChart, 2, 8);
            layout.add(queueToCarWashSizeChart, 1, 16);
            layout.add(queueToCashDeskChart, 2, 16);
            val scene = new Scene(layout);
            stage.setScene(scene);
            stage.show();
        });

        queueToDistributorSizeChart.getData().add(queueSizeSeries);
        waitingTimeToDistributorChart.getData().add(timeSeries);
        queueToCarWashSizeChart.getData().add(queueCarWashSizeSeries);
        queueToCashDeskChart.getData().add(queueCashDeskSeries);
    }

    public void updateGUI(int queueSizeToDistributor, double waitingTimeToDistributor,
                          int queueSizeToCarWash, int queueSizeToCashDesk,
                          double time) {
        val avgTimeDistributorString = GUIFederate.timeToDistributor + waitingTimeToDistributor;
        val avgSizeDistributorString = GUIFederate.queueSizeToDistributor + queueSizeToDistributor;
        //  val avgTimeCarWashString = GUIFederate.timeToCarWash + waitingTimeToCarWash;
        val avgSizeCarWashString = GUIFederate.queueSizeToCarWash + queueSizeToCarWash;
        val avgSizeCashDeskString = GUIFederate.queueSizeToCashDesk + queueSizeToCashDesk;
        log("Rozmiar kolejki wynosi do dystrybutora wynosi " + queueSizeToDistributor + ", a średni czas oczekiwania do dystrybutora " + waitingTimeToDistributor);
        runThread(avgTimeDistributorString, avgSizeDistributorString,
                waitingTimeToDistributor, queueSizeToDistributor,
                avgSizeCashDeskString, avgSizeCarWashString, queueSizeToCarWash, queueSizeToCashDesk);
    }

    private void runThread(String avgTimeDistributorString, String avgLengthDistributorString,
                           double waitingTimeToDistributor, int queueSizeToDistributor,
                           String avgTimeCashDeskString, String avgSizeCarWashString,
                           int queueSizeToCarWash, int queueSizeToCashDesk) {
        Platform.runLater(() -> {
            avgTimeLabelToDistributor.setText(avgTimeDistributorString);
            avgQueueSizeLabelToDistributor.setText(avgLengthDistributorString);
            avgQueueSizeLabelToCarWash.setText(avgSizeCarWashString);
            avgQueueSizeLabelToCashDesk.setText(avgTimeCashDeskString);
            if (queueSizeToDistributor != lastQueueSizeToDistributor) {
                queueSizeSeries.getData().add(new LineChart.Data(guiAmbassador.federateTime, queueSizeToDistributor));
                lastQueueSizeToDistributor = queueSizeToDistributor;
            }
            if (waitingTimeToDistributor != lastWaitingTimeAvgToDistributor) {
                timeSeries.getData().add(new LineChart.Data(guiAmbassador.federateTime, waitingTimeToDistributor));
                lastWaitingTimeAvgToDistributor = waitingTimeToDistributor;
            }
            if (queueSizeToCarWash != lastQueueSizeToCarWash) {
                queueCarWashSizeSeries.getData().add(new LineChart.Data(guiAmbassador.federateTime, queueSizeToCarWash));
                lastQueueSizeToCarWash = queueSizeToCarWash;
            }
            if (queueSizeToCashDesk != lastQueueSizeToCashDesk) {
                queueCashDeskSeries.getData().add(new LineChart.Data(guiAmbassador.federateTime, queueSizeToCashDesk));
                lastQueueSizeToCashDesk = queueSizeToCashDesk;
            }
        });
    }

    private void log(String message) {
        System.out.println("GUIFederate: " + message);
    }

    private void waitForUser() {
        log(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        val reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            log("Error while waiting for user input: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void enableTimePolicy() throws RTIexception {
        val currentTime = convertTime(guiAmbassador.federateTime);
        val lookahead = convertInterval(guiAmbassador.federateLookahead);

        this.rtIambassador.enableTimeRegulation(currentTime, lookahead);
        while (!guiAmbassador.isRegulating) rtIambassador.tick();

        this.rtIambassador.enableTimeConstrained();
        while (!guiAmbassador.isConstrained) rtIambassador.tick();
    }

    private void publishAndSubscribe() throws RTIexception {
        val classHandle = rtIambassador.getObjectClassHandle("ObjectRoot.Statistics");
        val queueSizeToDistributorHandle = rtIambassador.getAttributeHandle("queueSizeToDistributor", classHandle);
        val waitingTimeToDistributorHandle = rtIambassador.getAttributeHandle("waitingTimeToDistributor", classHandle);
        val queueSizeToCarWashHandle = rtIambassador.getAttributeHandle("queueSizeToCarWash", classHandle);
        val queueSizeToCashDeskHandle = rtIambassador.getAttributeHandle("queueSizeToCashDesk", classHandle);


        val attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(queueSizeToDistributorHandle);
        attributes.add(waitingTimeToDistributorHandle);
        attributes.add(queueSizeToCarWashHandle);
        attributes.add(queueSizeToCashDeskHandle);
        rtIambassador.subscribeObjectClassAttributes(classHandle, attributes);
    }

    private void advanceTime(double timeStep) throws RTIexception {
        guiAmbassador.isAdvancing = true;
        val newTime = convertTime(guiAmbassador.federateTime + timeStep);
        rtIambassador.timeAdvanceRequest(newTime);
        while (guiAmbassador.isAdvancing) rtIambassador.tick();
    }

    @Override
    public void start(Stage primaryStage) {
    }
}
