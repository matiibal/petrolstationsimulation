package org.example.util;

import hla.rti.LogicalTime;
import hla.rti.LogicalTimeInterval;
import org.portico.impl.hla13.types.DoubleTime;
import org.portico.impl.hla13.types.DoubleTimeInterval;

public class PorticoUtil {
    public static LogicalTime convertTime(double time) {
        return new DoubleTime(time);
    }

    public static LogicalTimeInterval convertInterval(double time) {
        return new DoubleTimeInterval(time);
    }

    public static double convertTimeAmbassador(LogicalTime logicalTime) {
        return ((DoubleTime) logicalTime).getTime();
    }
}
