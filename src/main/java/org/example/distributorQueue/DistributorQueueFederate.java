package org.example.distributorQueue;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.example.dao.Customer;
import org.example.dao.ExternalEvent;
import org.portico.impl.hla13.types.DoubleTime;
import org.portico.impl.hla13.types.DoubleTimeInterval;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.Random;

import static org.example.util.PorticoUtil.convertInterval;
import static org.example.util.PorticoUtil.convertTime;

public class DistributorQueueFederate {
    public static final String READY_TO_RUN = "ReadyToRun";
    private final double timeStep = 1.0;
    protected LinkedList<Customer> customerList = new LinkedList<>();
    protected int timeHlaHandle = 0;
    protected int queueHlaHandle = 0;
    private RTIambassador rtiAmbassador;
    private DistributorQueueAmbassador distributorQueueAmbassador;

    public static void main(String[] args) {
        try {
            new DistributorQueueFederate().runFederate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void runFederate() throws Exception {

        rtiAmbassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();

        try {
            val fom = new File("station.fed");
            rtiAmbassador.createFederationExecution("Federation - Station", fom.toURI().toURL());
            log("Created Federation");
        } catch (FederationExecutionAlreadyExists exists) {
            log("Didn't create federation, it already existed");
        } catch (MalformedURLException urle) {
            log("Exception processing fom: " + urle.getMessage());
            urle.printStackTrace();
            return;
        }

        distributorQueueAmbassador = new DistributorQueueAmbassador(this);
        rtiAmbassador.joinFederationExecution("DistributorQueueFederate", "Federation - Station", distributorQueueAmbassador);
        log("Joined Federation as org.example.customerQueue.CustomerQueueFederate");

        rtiAmbassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);

        while (!distributorQueueAmbassador.isAnnounced) {
            rtiAmbassador.tick();
        }

        waitForUser();

        rtiAmbassador.synchronizationPointAchieved(READY_TO_RUN);
        log("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");
        while (!distributorQueueAmbassador.isReadyToRun) {
            rtiAmbassador.tick();
        }

        enableTimePolicy();

        publishAndSubscribe();

        registerObject();

        while (distributorQueueAmbassador.running) {
            var timeToAdvance = distributorQueueAmbassador.federateTime + timeStep;
            advanceTime(timeToAdvance);
            if (distributorQueueAmbassador.externalEvents.size() > 0) {
                distributorQueueAmbassador.externalEvents.sort(new ExternalEvent.ExternalEventComparator());
                for (ExternalEvent externalEvent : distributorQueueAmbassador.externalEvents) {
                    distributorQueueAmbassador.federateTime = externalEvent.getTime();
                    switch (externalEvent.getEventType()) {
                        case ADD_CUSTOMER_TO_DISTRIBUTOR_QUEUE:
                            addCustomerToDistributorQueue(externalEvent.getCustomerId());
                            break;
                        case DELETE_CUSTOMER_FROM_DISTRIBUTOR_QUEUE:
                            if (!customerList.isEmpty()) {
                                if (isGoToWashCar()) {
                                    addCustomerToQueue(timeToAdvance + timeStep, "CarWash");
                                } else {
                                    addCustomerToQueue(timeToAdvance + timeStep, "CashDesk");
                                }
                                getCustomerFromQueue(timeToAdvance + timeStep);
                            }
                            break;
                    }
                }
                distributorQueueAmbassador.externalEvents.clear();
            }
            if (distributorQueueAmbassador.grantedTime == timeToAdvance) {
                timeToAdvance += distributorQueueAmbassador.federateLookahead;
                updateQueueSize(timeToAdvance);
                distributorQueueAmbassador.federateTime = timeToAdvance;
            }
            rtiAmbassador.tick();
        }
    }



    private boolean isGoToWashCar() {
        val random = new Random(); // creating Random object
        return random.nextBoolean();
    }


    private void getCustomerFromQueue(double time) throws RTIexception {
        if (customerList.size() > 0) {
            val customer = customerList.getFirst();
            customer.setRemoveTime(distributorQueueAmbassador.federateTime);
            customer.calculateWaitingTime();
            val waitingTime = customer.getWaitingTime();
            this.updateWaitingTime(time, waitingTime);
            customerList.removeFirst();
            log("Usuwamy klienta z kolejki, aktualny stan kolejki: " + customerList.size());
        }
    }

    private void addCustomerToDistributorQueue(int idCustomer) {
        val customer = new Customer(idCustomer, distributorQueueAmbassador.federateTime);
        customerList.add(customer);
        log("Dodajemy klienta do dystrybutora: " + customerList.size());
    }


    @SneakyThrows
    private void addCustomerToQueue(double timeToAdvance, String nameQueue) {
        val customer = customerList.getFirst();
        val parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();
        byte[] idCustomer = EncodingHelpers.encodeInt(customer.getId());
        int interactionHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.AddCustomerTo" + nameQueue + "Queue");
        int customerHandle = rtiAmbassador.getParameterHandle("idCustomer", interactionHandle);

        parameters.add(customerHandle, idCustomer);
        LogicalTime time = convertTime(timeToAdvance);
        rtiAmbassador.sendInteraction(interactionHandle, parameters, "tag".getBytes(), time);

        log("Dodanie klienta do kolejki " + nameQueue + ", liczba osób w kolejce: " + customerList.size());
    }

    private void updateQueueSize(double time) throws RTIexception {
        log("Liczba osób w kolejce do dystrybutora: " + customerList.size());
        val attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();

        val classHandle = rtiAmbassador.getObjectClass(queueHlaHandle);
        val queueHandle = rtiAmbassador.getAttributeHandle("size", classHandle);
        val queueSize = EncodingHelpers.encodeInt(customerList.size());

        attributes.add(queueHandle, queueSize);
        val logicalTime = convertTime(time);
        rtiAmbassador.updateAttributeValues(queueHlaHandle, attributes, "actualize queue".getBytes(), logicalTime);
    }

    private void updateWaitingTime(double time, double waitingTime) throws RTIexception {
        val attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();
        val classHandle = rtiAmbassador.getObjectClass(timeHlaHandle);
        val timeHandle = rtiAmbassador.getAttributeHandle("time", classHandle);
        val waitingTimeByte = EncodingHelpers.encodeDouble(waitingTime);
        attributes.add(timeHandle, waitingTimeByte);
        val logicalTime = convertTime(time);
        rtiAmbassador.updateAttributeValues(timeHlaHandle, attributes, "actualize queue".getBytes(), logicalTime);
    }


    private void registerObject() throws RTIexception {
        var classHandle = rtiAmbassador.getObjectClassHandle("ObjectRoot.QueueDistributor");
        this.queueHlaHandle = rtiAmbassador.registerObjectInstance(classHandle);

        classHandle = rtiAmbassador.getObjectClassHandle("ObjectRoot.WaitingTimeToDistributor");
        this.timeHlaHandle = rtiAmbassador.registerObjectInstance(classHandle);
    }

    private void publishAndSubscribe() throws RTIexception {
        var classHandle = rtiAmbassador.getObjectClassHandle("ObjectRoot.QueueDistributor");
        var sizeHandle = rtiAmbassador.getAttributeHandle("size", classHandle);

        AttributeHandleSet attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(sizeHandle);

        rtiAmbassador.publishObjectClass(classHandle, attributes);

        classHandle = rtiAmbassador.getObjectClassHandle("ObjectRoot.WaitingTimeToDistributor");
        sizeHandle = rtiAmbassador.getAttributeHandle("time", classHandle);

        attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(sizeHandle);

        rtiAmbassador.publishObjectClass(classHandle, attributes);

        val addCustomerHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.AddCustomerToDistributorQueue");
        distributorQueueAmbassador.addCustomerHandle = addCustomerHandle;
        rtiAmbassador.subscribeInteractionClass(addCustomerHandle);

        val getCustomerHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.GetCustomer");
        distributorQueueAmbassador.getCustomerHandle = getCustomerHandle;
        rtiAmbassador.subscribeInteractionClass(getCustomerHandle);

        val addCarWashHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.AddCustomerToCarWashQueue");
        rtiAmbassador.publishInteractionClass(addCarWashHandle);

        val addCashDeskHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.AddCustomerToCashDeskQueue");
        rtiAmbassador.publishInteractionClass(addCashDeskHandle);


    }

    private void advanceTime(double timeToAdvance) throws RTIexception {
        distributorQueueAmbassador.isAdvancing = true;
        val newTime = convertTime(timeToAdvance);
        rtiAmbassador.timeAdvanceRequest(newTime);
        while (distributorQueueAmbassador.isAdvancing) {
            rtiAmbassador.tick();
        }
    }

    private void enableTimePolicy() throws RTIexception {
        val currentTime = convertTime(distributorQueueAmbassador.federateTime);
        val lookahead = convertInterval(distributorQueueAmbassador.federateLookahead);
        this.rtiAmbassador.enableTimeRegulation(currentTime, lookahead);
        while (!distributorQueueAmbassador.isRegulating) rtiAmbassador.tick();

        this.rtiAmbassador.enableTimeConstrained();
        while (!distributorQueueAmbassador.isConstrained) rtiAmbassador.tick();
    }

    private void waitForUser() {
        log(" >>>>>>>>>> Press Enter to Continue <<<<<<<<<<");
        val reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (Exception e) {
            log("Error while waiting for user input: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void log(String message) {
        System.out.println("QueueFederate: " + message);
    }

}
